# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
#
# Author: Gerald Baulig


def delta():
	from time import time
	if hasattr(delta, 't'):
		t = time() - delta.t
	else:
		t = time()
	delta.t = time()
	return t


def load_json(json_file):
	from io import IOBase
	import json
	if type(json_file) is str:
		if json_file.endswith('.json'):
			with open(json_file, 'r', encoding='utf8') as fid:
				json_file = json.load(fid)
		else:
			json_file = json.loads(json_file)
	elif isinstance(json_file, IOBase):
		json_file = json.load(json_file)
	elif type(json_file) is dict:
		pass
	return json_file


class Label():
	idx = 0
	name = ''
	color = (0,0,0)
	tolrnc = (0,0,0)
	
	def __init__(self, idx=0, name='', color=(0,0,0), tolrnc=(0,0,0), parent=0):
		self.idx = idx
		self.name = name
		self.color = tuple([c/255 for c in color])
		self.tolrnc = tuple(tolrnc)
		self.parent = parent


def load_labels(json_file, idx_k='idx', name_k='name', color_k='color', tolrnc_k='tolrnc', parent_k='parent'):
	json_file = load_json(json_file)
	return {name:Label( \
		label[idx_k], \
		name, \
		label[color_k], \
		label[tolrnc_k], \
		label[parent_k]) \
		for name, label in json_file.items()}


class Event():
	def __init__(self):
		self.__handlers = []
		__iadd__ = self.add
		__contains__ = self.contains
		__isub__ = self.remove
		__call__ = self.emit

	def add(self, func):
		self.__handlers.append(func)
		return self

	def contains(self, func):
		return func in self.__handlers

	def remove(self, func):
		self.__handlers.remove(func)
		return self

	def emit(self, emitter, **kwargs):
		for hander in self.__handlers:
			hander(emitter, kwargs)
		pass

