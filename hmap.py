# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
#
# Author: Gerald Baulig

##INSTALLED##
import numpy as np


def gaussian_img(img_height, img_width, c_x, c_y, variance):
	gaussian_map = np.zeros((img_height, img_width))
	for x_p in range(img_width):
		for y_p in range(img_height):
			dist_sq = (x_p - c_x) * (x_p - c_x) + \
					  (y_p - c_y) * (y_p - c_y)
			exponent = dist_sq / 2.0 / variance / variance
			gaussian_map[y_p, x_p] = np.exp(-exponent)
	return gaussian_map


def create_gaussian(size, fwhm=3, center=None):
	""" Create a square gaussian kernel.
	size is the length of a side of the square
	fwhm is full-width-half-maximum, which
	can be thought of as an effective radius.
	"""

	x = np.arange(0, size, 1, float)
	y = x[:, np.newaxis]

	if center is None:
		x0 = y0 = size // 2
	else:
		x0 = center[0]
		y0 = center[1]

	return np.exp(-((x - x0) ** 2 + (y - y0) ** 2) / 2.0 / fwhm / fwhm)


def create_heatmaps(size, uvg_list):
	heatmaps = []
	for u, v, g in uvg_list:
		heatmaps.append(create_gaussian(size, g, center=(u*size, size-v*size)))
	return np.transpose(heatmaps, (1,2,0))


def find_maxima(hmaps, as_uvd=False):
	shape = hmaps.shape
	maxima = np.empty((shape[-1], 3))
	for i in range(shape[-1]):
		idx = np.argmax(hmaps[:,:,i])
		m, n = np.unravel_index(idx, shape[:2])
		d = hmaps[m,n,i]
		if as_uvd:
			m, n = (n / shape[0], 1 - m / shape[1])
		maxima[i,:] = (m, n, d)
	return maxima


def sum_hmaps(hmaps, labels=None):
	shape = hmaps.shape
	if labels:
		canvas = np.zeros((shape[0], shape[1], 3))
		for label in labels.values():
			idx = label.idx
			color = label.color
			for i, c in enumerate(color[::-1]):
				canvas[:,:,i] = canvas[:,:,i] + hmaps[:,:,idx] * c
	else:
		canvas = np.zeros((shape[0], shape[1], 1))
		for i in range(shape[2]):
			canvas[:,:,0] = canvas[:,:,0] + hmaps[:,:,i]
	return canvas

