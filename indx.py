#!/usr/bin/env python3


###BUILDIN###
import os
from glob import glob
from io import IOBase
from sys import stdout


def argparser(parents=None, prog=__file__):
	from argparse import ArgumentParser
	parser = ArgumentParser(
		parents=parents if parents else [],
		prog=prog,
		description='Prints an index of file relations of a given dataset.',
		epilog='Set a root DIR to the dataset and some wildcards of filenames listed column wise to the index file.'
	)
	parser.add_argument('output', help='Name or path for index file. If not set, strout will be used.', nargs='?', metavar='{FILE|PATH}')
	parser.add_argument('-0', '--col0', help='Wildcards for column 0 of index.', metavar='{COL0}', required=True)
	parser.add_argument('-1', '--col1', help='Wildcards for column 1 of index.', metavar='{COL1}')
	parser.add_argument('-2', '--col2', help='Wildcards for column 2 of index.', metavar='{COL2}')
	parser.add_argument('-3', '--col3', help='Wildcards for column 3 of index.', metavar='{COL3}')
	parser.add_argument('-4', '--col4', help='Wildcards for column 4 of index.', metavar='{COL4}')
	parser.add_argument('-5', '--col5', help='Wildcards for column 5 of index.', metavar='{COL5}')
	parser.add_argument('-6', '--col6', help='Wildcards for column 6 of index.', metavar='{COL6}')
	parser.add_argument('-7', '--col7', help='Wildcards for column 7 of index.', metavar='{COL7}')
	parser.add_argument('-8', '--col8', help='Wildcards for column 8 of index.', metavar='{COL8}')
	parser.add_argument('-9', '--col9', help='Wildcards for column 9 of index.', metavar='{COL9}')
	return parser

	
def write_index(columns, output=None):
	def process(stream):
		index = [sorted(glob(col)) for col in columns]
		for i in range(len(index[0])):
			row = [os.path.abspath(col[i]) for col in index]
			stream.write(' '.join(row) + '\n')
		stream.flush()

	if type(output) is str:
		with open(output, 'w') as fid:
			process(fid)
	elif isinstance(output, IOBase):
		process(output)
	else:
		process(stdout)


#MAIN---------------------------------------------#
def main(args):
	args = vars(args)
	columns = []
	for i in sorted(args.keys()):
		if args[i] and i.startswith('col'):
			columns.append(args[i])
	write_index(columns, args.get('output', None))


if __name__ == '__main__':
	args, _ = argparser().parse_known_args()
	main(args)
	
