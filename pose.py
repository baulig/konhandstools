# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
#
# Author: Gerald Baulig

###BUILDIN###
import math
###INSTALLED###
import numpy as np
import cv2


def fov_to_focal(fa, width=1.0, deg=False):
	if deg:
		fa = fa * math.pi / 180
	return width / math.tan(fa/2)


def fov_to_intrinsic(fa, fb=None, width=1.0, height=1.0, s=0, deg=False):
	if not fb:
		fb = fa
	fx = fov_to_focal(fa, width, deg)
	fy = fov_to_focal(fb, height, deg)
	K = np.array((
		(fx, s, width/2),
		(0, fy, height/2),
		(0, 0, 1)
	))
	return K


def focal_to_intrinsic(focal, s=0):
	K = np.array((
		(focal, s, 1),
		(0, focal, 1),
		(0, 0, 1)
	))
	return K


def rectify(uv1, uv2, focal=1.0, method=cv2.RANSAC, prob=0.99, threshold=1.0, s=0):
	'''https://stackoverflow.com/questions/33906111/how-do-i-estimate-positions-of-two-cameras-in-opencv'''
	K = focal_to_intrinsic(focal, s)
	E, emask = cv2.findEssentialMat(uv1, uv2, focal=focal, pp=(-0.5,0.5), method=method, prob=prob, threshold=threshold)	
	_, R2, T2, _ = cv2.recoverPose(E, uv1, uv2, focal=focal, pp=(-0.5,0.5), mask=emask)
	R1 = np.eye(3,3)
	T1 = np.zeros((3,1))

	M1 = np.hstack((R1, T1))
	M2 = np.hstack((R2, T2))
	P1 = np.dot(K, M1)
	P2 = np.dot(K, M2)

	points4D = cv2.triangulatePoints(P1, P2, uv1.transpose(), uv2.transpose())
	points3D = cv2.convertPointsFromHomogeneous(points4D.transpose())[:,0,:]

	return points3D, (T1[:,0], T2[:,0])


def camera_pose(H):
	'''https://stackoverflow.com/a/27138918'''
	H1 = H[:, 0]
	H2 = H[:, 1]

	norm1 = np.linalg.norm(H1)
	norm2 = np.linalg.norm(H2)
	tnorm = (norm1 + norm2) / 2.0;

	#H1 = H1 / norm1
	#H2 = H2 / norm1
	H3 = np.cross(H1, H2)

	T = H[:, 2] / tnorm
	return np.array([H1, H2, H3, T]).transpose()


def rectify2(uv1, uv2, aspect=(1,1), method=cv2.RANSAC, accuracy=1, prob=0.99, threshold=3):
	fmat, mask = cv2.findFundamentalMat(uv1, uv2, method=cv2.RANSAC, param1=accuracy, param2=prob) #LMEDS
	good1 = np.expand_dims(uv1[mask[:,0]==1], axis=0)
	good2 = np.expand_dims(uv2[mask[:,0]==1], axis=0)
	good1, good2 = cv2.correctMatches(fmat, good1, good2)
	_, h1, h2 = cv2.stereoRectifyUncalibrated(good1, good2, fmat, aspect, threshold=thresh)

	cam1 = camera_pose(h1)
	cam2 = camera_pose(h2)

	points4D = cv2.triangulatePoints(cam1, cam2, uv1.transpose(), uv2.transpose())
	points3D = cv2.convertPointsFromHomogeneous(points4D.transpose())[:,0,:]

	_, _, cam1, _, _, _, _ = cv2.decomposeProjectionMatrix(cam1)
	_, _, cam2, _, _, _, _ = cv2.decomposeProjectionMatrix(cam2)
	cam1 = cv2.convertPointsFromHomogeneous(cam1.transpose())[0,0,:]
	cam2 = cv2.convertPointsFromHomogeneous(cam2.transpose())[0,0,:]
	return points3D, (cam1, cam2)
