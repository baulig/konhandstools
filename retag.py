#
# Author: Gerald Baulig

from . import load_json

class Target():
	def __init__(self, findall=None, find=None, get=None, text=None):
		self.findall = findall
		self.find = find
		self.get = get
		self.text = text
		pass


def load(retarget_json, findall_k='findall', find_k='find', get_k='get', text_k='text'):
	retarget_json = load_json(retarget_json)

	return {key:Target( \
		findall = target.get(findall_k, None), \
		find = target.get(find_k, None), \
		get = target.get(get_k, None), \
		text = target.get(text_k, None)) \
		for key, target in retarget_json.items()}


def insert(keypath, dict_pos, delimiter='/'):
	keypath = keypath.split(delimiter)
	for key in keypath[:-1]:
		if not key in dict_pos.keys():
			dict_pos[key] = {}
		dict_pos = dict_pos[key]
	return dict_pos, keypath[-1]


def attr(tags, attr_dict, text):
	def get_attr(tag):
		if attr_dict:
			result = {key:tag.get(target) for key, target in attr_dict.items()}
		else:
			result = {}
		if text:
			result[text] = tag.text
		return result
	
	if type(tags) is list:
		return [get_attr(tag) for tag in tags]
	else:
		return get_attr(tags)
	pass


def retarget(xml, retarget_dict, output=None, delimiter='/'):
	import xml.etree.ElementTree as XML
	if type(xml) is str:
		xml = XML.parse(xml).getroot()
	if type(retarget_dict) is str:
		retarget_dict = load(retarget_dict)
	if output is None:
		output = {}
	
	for keypath, target in retarget_dict.items():
		dict_pos, key = insert(keypath, output, delimiter)
		if target.findall:
			tags = xml.findall(target.findall)
		elif target.find:
			tags = xml.find(target.find)
	
		if target.get or target.text:
			dict_pos[key] = attr(tags, target.get, target.text)
	
	return output


def flatten(tree_dict, keep=None, delimiter='/'):
	def recursive(subdict, subkey=None):
		for key, val in subdict.items():
			if subkey:
				joinkey = delimiter.join([subkey, key])
			else:
				joinkey = key
			if not (type(val) is dict) or (key in keep):
				flat_dict[joinkey] = val
			else:
				recursive(val, joinkey)

	if keep is None:
		keep = []
	flat_dict = {}
	recursive(tree_dict)
	return flat_dict


class Retarget():
	def __init__(self,
			retarget_json=None, 
			findall_key='findall', 
			find_key='find', 
			get_key='get', 
			text_key='text', 
			delimiter='/'
		):
		self.findall_key = findall_key
		self.find_key = find_key
		self.get_key = get_key
		self.text_key = text_key
		self.delimiter = delimiter
		self.retarget_dict = None
		
		if retarget_json:
			self.load(retarget_json)

	def load(self, retarget_json):
		retarget_dict = load_retarget_dict(
			retarget_json, 
			self.findall_key, 
			self.find_key, 
			self.get_key, 
			self.text_key)
		self.retarget_dict = retarget_dict
		return retarget_dict

	def retarget(self, xml, retarget_dict=None):
		if type(retarget_dict) is str:
			retarget_dict = self.load(retarget_dict)
		else:
			retarget_dict = self.retarget_dict
		return retarget(xml, retarget_dict, self.delimiter)

