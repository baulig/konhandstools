# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
#
# Author: Gerald Baulig

###BUILDIN###
import math
##INSTALLED##
import numpy as np
import cv2
####LOCAL####
from .hmap import sum_hmaps


def read_image(src, boxsize):
	# from file
	if type(src) is str:
		in_img = cv2.imread(src)
	# from webcam or video
	elif isinstance(src, cv2.VideoCapture):
		_, in_img = src.read()
	else:
		raise ValueError('ERROR: src must be either str or instance of cv2.VideoCapture')

	shape = in_img.shape
	if shape[0] == boxsize and shape[1] == boxsize:
		uv_scale = 1.0/boxsize
		return in_img, uv_scale

	if shape[0] > shape[1]:
		scale = boxsize / shape[0]
		in_img = cv2.resize(in_img, (0, 0), fx=scale, fy=scale)
		uv_scale = 1.0/shape[0]
	else:
		scale = boxsize / in_img.shape[1]
		in_img = cv2.resize(in_img, (0, 0), fx=scale, fy=scale)
		uv_scale = 1.0/shape[1]

	out_img = np.zeros((boxsize, boxsize, 3))
	out_img[:in_img.shape[0], :in_img.shape[1], :] = in_img
	return out_img, uv_scale


def draw_hmaps(img, hmaps, labels=None, img_ratio=0.5, hmap_ratio=0.5):
	shape = img.shape
	canvas = sum_hmaps(hmaps, labels)
	canvas = np.clip(canvas*255, 0, 255) 
	canvas = cv2.resize(canvas, shape[:2])
	return np.clip(canvas*hmap_ratio + img*img_ratio, 0, 255).astype(np.uint8)


def draw_joints(canvas, uvd, labels, line=1, as_rcd=False, colscl=1.0, min_d=0):
	mnd = uvd.copy()
	if as_rcd:
		mnd[:,:2] = uvd[:,1::-1]
	else:
		mnd[:,0] *= canvas.shape[1]
		mnd[:,1] = canvas.shape[0] - mnd[:,1] * canvas.shape[0]
	for label in labels.values():
		idx = label.idx
		if colscl != 1.0:
			color = np.array(label.color) * colscl
		else:
			color = label.color
		p_idx = label.parent
		if min_d > mnd[idx,2]:
			continue
		if min_d > mnd[p_idx,2]:
			continue
		cv2.line(canvas, tuple(mnd[idx,:2].astype(np.int)), tuple(mnd[p_idx,:2].astype(np.int)), color[::-1], line)
	return canvas


def draw_arrows(canvas, uvd1, uvd2, labels, selected=None, color=(0,0,0), line=1, as_rcd=False, colscl=1.0):
	mnd1 = uvd1.copy()
	mnd2 = uvd2.copy()
	if as_rcd:
		mnd1[:,:2] = uvd1[:,1::-1]
		mnd2[:,:2] = uvd2[:,1::-1]
	else:
		mnd1[:,0] *= canvas.shape[1]
		mnd1[:,1] = canvas.shape[0] - mnd1[:,1] * canvas.shape[0]	
		mnd2[:,0] *= canvas.shape[1]
		mnd2[:,1] = canvas.shape[0] - mnd2[:,1] * canvas.shape[0]
	if not labels:
		for p1, p2 in zip(mnd1[:,:2], mnd2[:,:2]):
			cv2.arrowedLine(canvas, tuple(p1.astype(np.int)), tuple(p2.astype(np.int)), color[::-1], line)
	elif not selected:
		selected = labels.keys()
	for idx1, key in enumerate(selected):
		idx2 = labels[key].idx
		if colscl != 1.0:
			color = np.array(labels[key].color) * colscl
		else:
			color = labels[key].color
		p1 = mnd1[idx1,:2]
		p2 = mnd2[idx2,:2]
		cv2.arrowedLine(canvas, tuple(p1.astype(np.int)), tuple(p2.astype(np.int)), color[::-1], line)
	return canvas


def show(imgname, canvas, keys=None, wait=1000):
	cv2.imshow(imgname, canvas)
	key = cv2.waitKey(wait)
	if not keys:
		return key
	while key not in keys:
		cv2.imshow(imgname, canvas)
		key = cv2.waitKey(wait)
	return key


def save(imgname, canvas):
	cv2.imwrite(imgname, canvas)

