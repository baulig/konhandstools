# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
#
# Author: Gerald Baulig

##INSTALLED##
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def calc_pck(report, xs=None, bins=20):
	def calc_y(x):
		y = sum(1 for r in report if r <= x) / report_len
		return y

	report_len = len(report)
	report_max = max(report)
	if xs is None:
		xs = np.arange(0.0, report_max, report_max/bins)
	ys = [calc_y(x) for x in xs]
	return xs, ys


def plot_pck(report, label='pck', style='b-', xs=None, bins=20):
	def show(title='PCK'):
		plt.title(title)
		plt.legend()
		plt.xlabel('norm eucl dist threshold')
		plt.ylabel('% of examples')
		plt.show()

	def add(report, label='pck', style='b-', xs=None, bins=20):
		xs, ys = calc_pck(report, xs, bins)
		plt.plot(xs, ys, style, label=label)
		return plot_pck

	plot_pck.show = show
	plot_pck.add = add
	return add(report, label, style, xs, bins)


class JointPlot():
	def __init__(self, labels, **events):
		self.labels = labels
		self.__ax = plt.figure().gca(projection='3d')
		
		for key, event in events.items():
			plt.connect(key, event)
		pass
	
	def update(self, uvd, cams=None, minax=(0,0,0), maxax=(1,1,1), fontsize=8):
		mpl.rcParams['legend.fontsize'] = fontsize
		self.__ax.clear()
		self.__ax.set_xlim(minax[0], maxax[0])
		self.__ax.set_ylim(minax[1], maxax[1])
		self.__ax.set_zlim(minax[2], maxax[2])
		self.__ax.set_xlabel('X axis')
		self.__ax.set_ylabel('Z axis')
		self.__ax.set_zlabel('Y axis')
		for label in self.labels.values():
			u = [uvd[int(label.parent), 0], uvd[int(label.idx), 0]]
			v = [uvd[int(label.parent), 1], uvd[int(label.idx), 1]]
			d = [uvd[int(label.parent), 2], uvd[int(label.idx), 2]]
			self.__ax.plot(u, v, zs=d, zdir='y', color=label.color, label=label.name)
		if cams:
			for idx, cam in enumerate(cams):
				u = [cam[0], uvd[0, 0]]
				v = [cam[1], uvd[0, 1]]
				d = [cam[2], uvd[0, 2]]
				self.__ax.plot(u, v, zs=d, zdir='y', color=(1-1/(idx+1),)*3, label='cam{}'.format(idx))
		self.__ax.legend()
		pass

	def show(self):
		plt.show()

	def close(self):
		plt.close('all')


